<?php
require 'vendor/autoload.php';

$birthday = explode('-',$_POST['birthday']);
$dia = $birthday[2];
$mes = $birthday[1];
$ano = $birthday[0];

$dataSet = [
    "claveEmpresa" => "000100000000",
    "usuarioConsulta" => "000000000043",
    "diasVigencia" => "30",
    "transaccionId" => null,
    "origen" => "FINDEPMOVIL",
    "personas" => [
        [
            "encabezado" => [
                "productoRequerido" => 007 //Ver documentación de productos disponibes en
                // documentación oficial del proveedor (Cada producto está sujeto a previa activación por el proveedor, Es necesario revisar si el producto requerido está habilitado).
            ],
            "nombre" => [
                "apellidoPaterno" => $_POST['lastname'],
                "apellidoMaterno" => $_POST['mothername'],
                "apellidoAdicional" => null,
                "primerNombre" => $_POST['firstname'],
                "segundoNombre" => null,
                "fechaNacimiento" => $dia.$mes.$ano,
                "rfc" => $_POST['rfc'],
                "prefijo" => null,
                "sufijo" => null,
                "nacionalidad" => null,
                "residencia" => null,
                "numeroLicenciaConducir" => null,
                "estadoCivil" => null,
                "sexo" => $_POST['gender'],
                "numeroCedulaProfesional" => null,
                "numeroRegistroElectoral" => null,
                "claveImpuestosOtroPais" => null,
                "claveOtroPais" => null,
                "numeroDependientes" => null,
                "edadesDependientes" => null
            ],
            "domicilios" => [
                [
                    "direccion1" => $_POST['street'],
                    "direccion2" => null,
                    "coloniaPoblacion" => $_POST['settlement'],
                    "delegacionMunicipio" => $_POST['town'],
                    "ciudad" => $_POST['town'],
                    "estado" => $_POST['state'],
                    "cp" => $_POST['postalcode'],
                    "fechaResidencia" => null,
                    "numeroTelefono" => null,
                    "extension" => null,
                    "fax" => null,
                    "tipoDomicilio" => null,
                    "indicadorEspecialDomicilio" => null,
                    "codPais" => "MX"


                ]
            ],
            "autentica" => [
                "tarjetaCredito" => "V",
                "ultimosCuatroDigitos" => "3465",
                "ejercidoCreditoHipotecario" => "F",
                "ejercidoCreditoAutomotriz" => "V"

            ]
        ]
    ]
];

$BCS = new \GuzzleHttp\Client('https://buro-credito-service-dot-findep-desarrollo-170215.appspot.com/buro/entidadFisica/autenticar');
$dataBuro = $BCS->request('POST',[
    'json' => $dataSet
]);

