/* --------- header ---------- */
/* navbar */
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

/* testimonios */
$('.testimonios_carousel').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    // autoplay: true,
    autoplaySpeed: 2000,
    responsive: [{
        breakpoint: 576,
        settings: {
            slidesToShow: 1,
            rows: 6,
            slidesPerRow: 1
        },
    }, ]
});

$(document).ready(function() {
    $(".topnav a").hover(function() {
        $(".icon").css({ "background-color": "white", "color": "#3ec37a" })
    })

    //... video modal
    $(".video-play-button").click(function() {
        var urllink = $(this).parent().find('source').attr('src');
        $(".modal-content video").attr('src', urllink)
        $("#myModal").modal();
        alert(urllink)

    });


    $('.video-overlay, .video-overlay-close').on('click', function(e) {
        e.preventDefault();
        close_video();
    });

    $(document).keyup(function(e) {
        if (e.keyCode === 27) { close_video(); }
    });

    function close_video() {
        $('.video-overlay.open').removeClass('open').find('iframe').remove();
    };
    document.addEventListener('touchmove', this._preventDefault, { passive: false });

})
document.addEventListener('touchmove', this._preventDefault, { passive: false });


//... slider 01 , 02
$(document).ready(function() {

    $(".mb_slider").mbSlider({
        onSlide: function(o) {
            $(".mb_sliderValueLabel").text($(o).mbgetVal())
            update();
        },
        formatValue: function(val) {
            return val + ' meses';
        }
    });

    $("#slider1").slider({
        range: "min",
        animate: true,
        value: 1,
        min: 50,
        max: 1000,
        step: 1,
        slide: function(event, ui) {
            update(1, ui.value); //changed
        }
    });
    //Added, set initial value.
    $("#amount").val(50);
    $("#amount-label").text('$50,000MXN');
    update();
});

function PMT(ir, np, pv, fv, type) {
    var pmt, pvif;
    fv || (fv = 0);
    type || (type = 0);
    if (ir === 0)
        return -(pv + fv)/np;
    pvif = Math.pow(1 + ir, np);
    pmt = - ir * pv * (pvif + fv) / (pvif - 1);
    if (type === 1)
        pmt /= (1 + ir);
    return pmt;
}
function toCommas(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function update(slider, val) {
    var formatNumber = {
        separador: ",",
        sepDecimal: '.',
        formatear: function(num) {
            num += '';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
            }
            return this.simbol + splitLeft + splitRight;
        },
        new: function(num, simbol) {
            this.simbol = simbol || '';
            return this.formatear(num);
        }
    }
    var $amount = slider == 1 ? val : $("#amount").val();
    $("#amount").val($amount)
   
    if  ($("#amount").val()==1000) { $('#slider1 span').html("$1,000,000"); } else
    { $('#slider1 span').html("$" + $amount + ",000");}
    
    var value01 = parseInt($("#amount").val());
    var value02 = parseInt($('.mb_sliderValueLabel').text());
    if (value02 == 30) {
        value02 = 36;
    }


ir = 0.57 / 12;
np = value02 ;
pv = value01*1000;
pmt2 = PMT(ir, np, pv);
intereses=pv*ir;
iva=intereses*0.16;
    var calculation = (pmt2*(-1)+iva).toFixed(0);
    


    $(".total_price").text("$" + toCommas(calculation));
    
}