/* --------- header ---------- */
/* navbar */
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

/* testimonios */
$('.testimonios_carousel').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    // autoplay: true,
    autoplaySpeed: 2000,
    responsive: [{
        breakpoint: 576,
        settings: {
            slidesToShow: 1,
            rows: 6,
            slidesPerRow: 1
        },
    }, ]
});

$(document).ready(function() {
    $(".topnav a").hover(function() {
            $(".icon").css({ "background-color": "white", "color": "#3ec37a" })
        })
        //... video modal
    $(".video-play-button").click(function() {
        var urllink = $(this).parent().find('source').attr('src');
        $(".modal-content video").attr('src', urllink)
        $("#myModal").modal();
        alert(urllink)

    });
    $('.video-overlay, .video-overlay-close').on('click', function(e) {
        e.preventDefault();
        close_video();
    });
    // $(document).keyup(function(e) {
    //     if (e.keyCode === 27) { close_video(); }
    // });

    function close_video() {
        $('.video-overlay.open').removeClass('open').find('iframe').remove();
    };
    document.addEventListener('touchmove', this._preventDefault, { passive: false });
})
document.addEventListener('touchmove', this._preventDefault, { passive: false });


//... slider 01 , 02
$(document).ready(function() {
    $(".mb_slider").mbSlider({
        onSlide: function(o) {
            $(".mb_sliderValueLabel").text($(o).mbgetVal())
            update();
        },
        formatValue: function(val) {
            return val + ' meses';
        }
    });
    $("#slider1").slider({
        range: "min",
        animate: true,
        value: 1,
        min: 50,
        max: 250,
        step: 1,
        slide: function(event, ui) {
            update(1, ui.value); //changed
        }
    });
    //Added, set initial value.
    $("#amount").val(50);
    $("#amount-label").text('$50,000MXN');
    update();


    // JavaScript for label effects only

    $(".input-effect input").focusin(function() {
        $(this).parent().find('label').css({ "top": "-18px", "left": "0", "font-size": "12px", "color": "#3399", "transition": "0.3s" })
    })
    $(".input-effect input").focusout(function() {
        $(this).parent().find('label').css({ "top": "-18px", "left": "0", "font-size": "12px", "color": "#3399", "transition": "0.3s" })
    })

    //... number files validation with jquery

    $(".numberfield").bind("change keyup input", function() {
        var position = this.selectionStart - 1;
        //remove all but number and .
        var fixed = this.value.replace(/[^0-9]/g, "");

        if (this.value !== fixed) {
            this.value = fixed;
            this.selectionStart = position;
            this.selectionEnd = position;
        }
    });

    $(".datepicker-here").datepicker({
        offset: 12,
        onSelect: function(dateText) {
            console.log("Selected date: " + dateText + "; input's current value: " + this.value);
            $("#date-picker-year").text(dateText);
        },

    });

    //.. curp validation
    $("[type='text']").keypress(function(e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. Alphabets and Numbers.
        var regex =  /[ A-Za-zÃ±Ã¡Ã©Ã­Ã³ÃºñÑáéíóúÁÉÍÓÚ0-9]+$/;
        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            console.log('Only Alphabets and Numbers allowed.');
            return false;
        } else {
            return true;
        }
    });

    $("[type='number']").keypress(function(e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. Alphabets and Numbers.
        var regex = /^[0-9]+$/;
        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            console.log('Only Alphabets and Numbers allowed.');
            return false;
        } else {
            return true;
        }
    });

    $("[type='email']").keypress(function(e) {
        var keyCode = e.keyCode || e.which;
        //Regex for Valid Characters i.e. Alphabets and Numbers.
        var regex =  /[A-Z0-9a-z.@_.-]+$/;
        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            console.log('Only Alphabets and Numbers allowed.');
            return false;
        } else {
            return true;
        }
    });

});



function update(slider, val) {
    var formatNumber = {
        separador: ",",
        sepDecimal: '.',
        formatear: function(num) {
            num += '';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
            }
            return this.simbol + splitLeft + splitRight;
        },
        new: function(num, simbol) {
            this.simbol = simbol || '';
            return this.formatear(num);
        }
    }
    var $amount = slider == 1 ? val : $("#amount").val();
    $("#amount").val($amount)
    $('#slider1 span').html("$" + $amount + ",000 MXN");
    var value01 = parseInt($("#amount").val());
    var value02 = parseInt($('.mb_sliderValueLabel').text());
    if (value02 == 30) {
        value02 = 36;
    }
    var calculation = (value01 * 1000 * 1.57 / value02).toFixed(3);
    $(".total_price").text("$" + calculation);
}