$(document).ready(function() {
    // $('#datetimePicker').datetimepicker();

    $('.validateForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                firstname: {
                    message: ' ',
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: ' '
                        },
                        regexp: {
                            regexp: /^[ A-Za-zÃ±Ã¡Ã©Ã­Ã³ÃºñÑáéíóúÁÉÍÓÚ]+$/i,
                            message: ' '
                        }
                    }
                },
                lastname: {
                    message: ' ',
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            // min: 6,
                            max: 30,
                            message: ' '
                        },
                        regexp: {
                            regexp: /^[ A-Za-zÃ±Ã¡Ã©Ã­Ã³ÃºñÑáéíóúÁÉÍÓÚ]+$/i,
                            message: ' '
                        }
                    }
                },
                username: {
                    message: ' ',
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            // min: 6,
                            max: 30,
                            message: ' '
                        },
                        regexp: {
                            regexp: /^[ A-Za-zÃ±Ã¡Ã©Ã­Ã³ÃºñÑáéíóúÁÉÍÓÚ]+$/,
                            message: ' '
                        }
                    }
                },
                phonenumber: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 10,
                            max: 10,
                            message: ' '
                        },
                        regexp: {
                            regexp: /^[\w]+$/,
                            message: ' '
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        emailAddress: {
                            message: ' '
                        },
                        stringLength: {
                            min: 1,
                            max: 30,
                            message: ' '
                        },
                    }
                },
                verificationcode: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 1,
                            max: 5,
                            message: ' '
                        },
                         regexp: {
                             regexp: /^[A-Za-z0-9]+$/,
                             message: ' '
                         }
                    }
                },
                rfc: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 13,
                            max: 13,
                            message: ' '
                        },
                         regexp: {
                            regexp: /^[A-Za-z0-9]+$/,
                             message: ' '
                         }
                    }
                },
                curp: {
                    notEmpty: {
                        message: ' '
                    },
                    validators: {
                        stringLength: {
                            min: 18,
                            max: 18,
                            message: ' '
                        },
                    },
                     regexp: {
                        regexp: /^[A-Za-z0-9]+$/,
                         message: ' '
                     },
                },
                gender: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                },
                giro: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                },
                experience: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                },
                street: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 1,
                            max: 30,
                            message: ' '
                        },
                      //  regexp: {
                      //      regexp: /^[a-z\s_0-9]+$/i,
                      //      message: ' '
                      //  }
                    }
                },
                outdoor_number: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 1,
                            max: 5,
                            message: ' '
                        },
                      //  regexp: {
                     //       regexp: /^[\w]+$/,
                       //     message: ' '
//},
                    }
                },
                interior_number: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 1,
                            max: 5,
                            message: ' '
                        },
                      //  regexp: {
                        //    regexp: /^[\w]+$/,
                        //    message: ' '
                      //  },
                    }
                },
                postalcode: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 1,
                            max: 5,
                            message: ' '
                        },
                  //      regexp: {
                     //       regexp: /^[\w]+$/,
                   //         message: ' '
                    //    },
                    }
                },
                settlement: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 1,
                            max: 30,
                            message: ' '
                        },
                      //  regexp: {
                       //     regexp: /^[a-z\s_0-9]+$/i,
                      //      message: ' '
                      //  }
                    }
                },
                state: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 1,
                            max: 20,
                            message: ' '
                        },
                      //  regexp: {
                       //     regexp: /^[a-z\s_0-9]+$/i,
                        //    message: ' '
                       // }
                    }
                },
                town: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        },
                        stringLength: {
                            min: 1,
                            max: 20,
                            message: ' '
                        },
                    //    regexp: {
                      //      regexp: /^[a-z\s_0-9]+$/i,
                      //      message: ' '
                      //  }
                    }
                },
                age: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                },
                country: {
                    validators: {
                        notEmpty: {
                            message: 'The country is required and can\'t be empty'
                        }
                    }
                },
                acceptTerms: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                },
            }
        })
        .on('status.field.bv', function(e, data) {
            // I want to enable the submit button all the time
            data.bv.disableSubmitButtons(false);
        })
    $('#datetimePicker').on('dp.change dp.show', function(e) {
        $('.validateForm').bootstrapValidator('revalidateField', 'birthday');
    });

    // Validate the form manually
    $('.siguiente:not(:last-child())').click(function() {
        $('.validateForm').bootstrapValidator('validate');
        var nextform_number = $(this).attr('href');
        if ($('.validateForm').bootstrapValidator('validate').has('.has-error').length === 0) {
            console.log('success')
            currentSlide(nextform_number);
        } else {
            console.log('bad')
        }

    });
});